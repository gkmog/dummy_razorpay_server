const ngrok = require("ngrok");
const strigify = require("json-stringify");

const options = {
  proto: "http", // http|tcp|tls, defaults to http
  addr: 8000, // port or network address, defaults to 80
  region: "ap", // one of ngrok regions (us, eu, au, ap), defaults to us
  onStatusChange: status => {}, // 'closed' - connection is lost, 'connected' - reconnected
  onLogEvent: data => {} // returns stdout messages from ngrok process
};

(async function() {
  console.log("connecting to ngrok");
  try {
    let url = await ngrok.connect(options);
    console.log("connected ", url.toString());
  } catch (e) {
    console.error("ERR::\n" + strigify(e));
  }
})();

process.stdin.resume();
