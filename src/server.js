const http = require("http");
const url = require("url");
const razorpayurl = "https://api.razorpay.com/v1/orders";
const request = require("request");

const headers = {
  Authorization:
    "Basic " +
    Buffer.from("rzp_test_TcuWYPsuvhhZQo:mGbOQykVMAp6lNCKQlfyKHRh").toString(
      "base64"
    ),
  "Content-Type": "application/x-www-form-urlencoded"
};

const webhook = function(req, response) {
  console.log("webhook called");
  response.statusCode = 200;
  response.end("OK");
};

const handler = function(req, response) {
  if (req.url.startsWith("/generateorder")) {
    let amount = url.parse(req.url, true).query["amount"];
    let form = {
      amount: amount,
      currency: "INR",
      receipt: "rcptid 1",
      payment_capture: "1"
    };
    request.post(
      { url: razorpayurl, headers: headers, form: form, method: "POST" },
      function(e, r, body) {
        response.statusCode = 200;
        if (e) {
          console.error("Error occured ", e);
          response.end(e.toString());
          return;
        }
        response.statusCode = r.statusCode;
        if (body) {
          response.end(body.toString());
        } else {
          response.end("NO-CONTENT");
        }
      }
    );
    return;
  }
  if (req.url.startsWith("/webhook")) {
    webhook(req, response);
    return;
  }

  response.statusCode = 404;
  response.end();
};

function startServer() {
  http.createServer(handler).listen(8000);
}

startServer();
